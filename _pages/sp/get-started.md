---
layout: blocks
lang: sp
title: 'Get Started SP'
permalink: "/sp/get-started"
page_sections:
- template: HERO_GET_STARTED_block
  block: HERO_GET_STARTED_block
  teaser-2-title: Get Started SP
- template: THRESS_OPTIONS_block
  block: THRESS_OPTIONS_block
  go-head-bg: white
  go-head-title: HOW IT WORKS
  go-head-sub-title: Setup devices before going offline.
  go-head-list:
  - img-group:
    - img: "/assets/img/code.png"
    - img: "/assets/img/robot.png"
    title: Get F-Droid.
    text: Download F-Droid, an independent app store for Android. Scan QR code to
      start. You need internet for this step.
  - img-group:
    - img: "/assets/img/code.png"
    title: Get the Apps.
    text: Scan QR to add the Second Wind repo to F-Droid. It is a curatation of apps
      optimized for offline. You need internet for this step.
  - title: Choose How to Share.
    text: Choose your distribution method and begin sharing! You can share with F-Droid
      Nearby, jump drives or by setting up a hotspot.
    img-group: []
- template: VIDEO_block
  block: VIDEO_block
  video: https://www.youtube.com/embed/Xz5Tx7hNR64?autoplay=1
  video-img: "/assets/img/2020/12/01/thumb1.png"
  video-sub-title: TUTORIAL
  video-title: F-Droid Nearby
  video-accordion:
  - title: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  - title: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  - title: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  - title: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  - title: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- template: VIDEO_block
  block: VIDEO_block
  video: https://www.youtube.com/embed/NrmMk1Myrxc?autoplay=1
  video-img: "/assets/img/hqdefault-1.jpg"
  video-sub-title: TUTORIAL
  video-title: Jump Drives
  video-accordion:
  - title: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  - title: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  - title: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  - title: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  - title: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    text: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- template: CTA_block
  block: CTA_block
  about-title: Need help?
  about-paragraph: Contact Guardian Project with questions or to learn about getting
    pre-configured devices.
  about-btn-text: Reach Out
  about-btn-url: "#"
date: 

---
